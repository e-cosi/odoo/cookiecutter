# -*- coding: utf-8 -*-
# Copyright {% now 'utc', '%Y' %} {{cookiecutter.author}}
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import api, models


class {{cookiecutter.model_class_name}}WizardModel(models.TransientModel):
    _name = "{{ cookiecutter.module_dot }}.wizard_{{ cookiecutter.model_slug }}"

    @api.multi
    def action_accept(self):
        self.ensure_one()
        self.do_something_useful()