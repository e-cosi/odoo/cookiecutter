# -*- coding: utf-8 -*-
# Copyright {% now 'utc', '%Y' %} {{cookiecutter.author}}
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).


from odoo import api, models


class {{ cookiecutter.report_name }}Report(models.AbstractModel):
    _name = "report.{{ cookiecutter.module_dot }}.module.{{ cookiecutter.report_slug  }}"

    @api.multi
    def render_html(self, data=None):
        report_obj = self.env["report"]
        report = report_obj._get_report_from_name("{{ cookiecutter.module_dot }}.module.{{ cookiecutter.report_slug  }}")
        docargs = {
            "doc_ids": self._ids,
            "doc_model": report.model,
            "docs": self,
        }
        return report_obj.render("{{ cookiecutter.module_dot }}.module.{{ cookiecutter.report_slug  }}", docargs)