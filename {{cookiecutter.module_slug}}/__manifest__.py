# -*- coding: utf-8 -*-
# Copyright {% now 'utc', '%Y' %} {{cookiecutter.author}}
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "{{cookiecutter.module_name}}",
    "summary": "{{cookiecutter.summary}}",
    "version": "{{cookiecutter.odoo_version}}.{{cookiecutter.version}}",
    "development_status": "Alpha|Beta|Production/Stable|Mature",
{% if cookiecutter.price != "free" %}
    "price": {{ cookiecutter.price }},
    "currency": {{ cookiecutter.currency }},
{% endif %}
    "category": "{{cookiecutter.category}}",
    "website": "{{cookiecutter.website}}",
    "author": "{{cookiecutter.author}}",
    "contributors": [
        "{{cookiecutter.contributors}}",
    ]
    "license": "{{cookiecutter.licence}}",
{% if cookiecutter.application != "n" %}
    "application": True,
{% else %}
    "application": False,
{% endif %}
    # TODO
    "installable": True,
    # TODO
    "preloadable": True,
{% if cookiecutter.pre_init_hook %}
    "pre_init_hook": "pre_init_hook",
{% endif %}
{% if cookiecutter.post_init_hook %}
    "post_init_hook": "post_init_hook",
{% endif %}
{% if cookiecutter.post_load %}
    "post_load": "post_load",
{% endif %}
{% if cookiecutter.uninstall_hook %}
    "uninstall_hook": "uninstall_hook",
{% endif %}
    # "external_dependencies": {
    #     "python": [],
    #     "bin": [],
    # },
    # "depends": [
    #     "base",
    # ],
    "data": [
{% if cookiecutter.security != "n" %}
        "security/{{cookiecutter.model_slug}}_groups.xml",
        "security/{{cookiecutter.model_slug}}_security.xml",
        "security/ir.model.access.csv",
{% endif %}
{% if cookiecutter.views != "n" %}
        "views/{{cookiecutter.model_slug}}_views.xml",
{% endif %}
{% if cookiecutter.report != "n" %}
        "report/{{cookiecutter.report_slug}}_report_views.xml",
        "report/{{cookiecutter.report_slug}}_templates.xml",
        "report/{{cookiecutter.report_slug}}_reports.xml",
{% endif %}
{% if cookiecutter.wizard != "n" %}
        "wizard/{{cookiecutter.model_slug}}_views.xml",
{% endif %}
    ],
{% if cookiecutter.demo != "n" %}
    "demo": [
        "demo/{{cookiecutter.module_slug}}_demo.xml",
    ],
{% endif %}
}
