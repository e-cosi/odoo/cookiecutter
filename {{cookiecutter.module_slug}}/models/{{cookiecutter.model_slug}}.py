# -*- coding: utf-8 -*-
# Copyright {% now 'utc', '%Y' %} {{cookiecutter.author}}
{% if cookiecutter.licence == 'AGPL-3' %}
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{% elif cookiecutter.licence == 'OPL-1' %}
# License OPL-1.0 or later (https://www.odoo.com/documentation/user/12.0/legal/licenses/licenses.html).
{% endif %}

from odoo import models


class {{ cookiecutter.model_class_name }}(models.Model):
    _name = "{{ cookiecutter.model_name }}"
{% if cookiecutter.model_inherit %}
    _inherit = "{{ cookiecutter.model_inherit }}"
{% endif %}

    # TODO: contraints (SQL, python), ...

    # TODO: each field type exemple (compute, relations, ...)
    # name = fields.Char(required=True, string='Nom', help='Nom de la tâche')
    # active = fields.Boolean(string='Actif', help='Champ inactif')
    # description = fields.Text(string='Description', help='Description complémentaire de la tâche')
    # state = fields.Selection([('draft', 'Ouverte'), ('close', 'Fermée')], string='État')
