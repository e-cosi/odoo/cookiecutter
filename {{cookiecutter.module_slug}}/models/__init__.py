# -*- coding: utf-8 -*-
# Copyright {% now 'utc', '%Y' %} {{cookiecutter.author}}
{% if cookiecutter.licence == 'AGPL-3' %}
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{% elif cookiecutter.licence == 'OPL-1' %}
# License OPL-1.0 or later (https://www.odoo.com/documentation/user/12.0/legal/licenses/licenses.html).
{% endif %}

from . import {{cookiecutter.model_slug}}
