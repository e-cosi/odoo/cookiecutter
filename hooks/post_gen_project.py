import os
import shutil

TERMINATOR = "\x1b[0m"
WARNING = "\x1b[1;33m [WARNING]: "
INFO = "\x1b[1;33m [INFO]: "
HINT = "\x1b[3;33m"
SUCCESS = "\x1b[1;32m [SUCCESS]: "


def remove_dir(dir_path):
    if os.path.exists(dir_path):
        shutil.rmtree(dir_path)


def main():
    if "{{ cookiecutter.controllers }}" == "n":
        remove_dir('controllers')

    if "{{ cookiecutter.models }}" == "n":
        remove_dir('models')

    if "{{ cookiecutter.views }}" == "n":
        remove_dir('views')

    if "{{ cookiecutter.data }}" == "n":
        remove_dir('data')

    if "{{ cookiecutter.security }}" == "n":
        remove_dir('security')

    if "{{ cookiecutter.demo }}" == "n":
        remove_dir('demo')

    if "{{ cookiecutter.wizard }}" == "n":
        remove_dir('wizards')

    if "{{ cookiecutter.report }}" == "n":
        remove_dir('report')

    if "{{ cookiecutter.tests }}" == "n":
        remove_dir('tests')

    print(SUCCESS + "Module initialized, keep up the good work!" + TERMINATOR)


if __name__ == "__main__":
    main()